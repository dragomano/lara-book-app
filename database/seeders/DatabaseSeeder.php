<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Book;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Создаем роли, права и пользователей
        $this->call(PermissionSeeder::class);

        // Добавляем авторов
        $authors = Author::factory(200)->create();
        $authorIds = $authors->pluck('id')->toArray();

        // Добавляем книги и закрепляем за каждой одного или нескольких авторов
        Book::factory(1000)->create()->each(function ($book) use ($authorIds) {
            $authors = Arr::random($authorIds, rand(1, 3));
            $book->authors()->attach($authors);
        });
    }
}

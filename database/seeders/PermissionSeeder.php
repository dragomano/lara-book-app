<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Сбрасываем кэшированные роли и права
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // Создаем нужные права
        Permission::create(['name' => 'add books']);
        Permission::create(['name' => 'edit books']);
        Permission::create(['name' => 'delete books']);

        // Администратор имеет все права через правило Gate::before в AuthServiceProvider
        $adminRole = Role::create(['name' => 'admin']);

        // Создаем роль менеджера и присваиваем ему необходимые права
        $managerRole = Role::create(['name' => 'manager']);
        $managerRole->givePermissionTo('add books');
        $managerRole->givePermissionTo('edit books');

        // Создаем роль пользователя и присваиваем ему права на добавление книг
        $userRole = Role::create(['name' => 'user']);
        $userRole->givePermissionTo('add books');

        // Создаем администратора
        $admin = User::factory()->create([
            'name' => 'Admin',
            'email' => 'admin@test.com',
            'email_verified_at' => now(),
            'password' => bcrypt('admin@test.com')
        ]);
        $admin->assignRole($adminRole);

        // Создаем рандомных менеджеров
        $managers = User::factory(4)->create();
        $managers->each(function ($manager) use ($managerRole) {
            $manager->assignRole($managerRole);
        });

        // Создаем рандомных пользователей
        $users = User::factory(100)->create();
        $users->each(function ($user) use ($userRole) {
            $user->assignRole($userRole);
        });
    }
}

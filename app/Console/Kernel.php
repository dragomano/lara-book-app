<?php

namespace App\Console;

use App\Models\Book;
use App\Notifications\TelegramNotification;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Notification;
use NotificationChannels\Telegram\TelegramChannel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            Book::factory()->create();
        })
            ->everyMinute()
            ->onSuccess(function () {
                $book = Book::with('user')->latest()->first();

                Notification::route(TelegramChannel::class, config('services.telegram-bot-api')['group'])
                    ->notify(new TelegramNotification([
                        'text' => "Добавлена новая книга:\n«{$book->title}», {$book->user->name}"
                    ]));
            });
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

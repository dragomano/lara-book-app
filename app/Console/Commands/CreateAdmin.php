<?php

namespace App\Console\Commands;

use App\Models\User;
use Validator;
use Illuminate\Console\Command;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:admin {name} {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $validator = Validator::make(
            [
                'name' => $name = $this->argument('name'),
                'email' => $email = $this->argument('email'),
                'password' => $pass = $this->argument('password')
            ],
            [
                'name' => 'required|string|max:255',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|string|min:8'
            ]
        );

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->comment($error);
            }

            return;
        }

        $user = User::factory()->create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($pass)
        ]);

        $user->assignRole('admin');

        $this->table(
            ['Name', 'Email', 'Password'],
            [[$name, $email, $pass]]
        );

        $this->info('Admin created!');
    }
}

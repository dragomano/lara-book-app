<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Auth;
use Inertia\Inertia;

class BookController extends Controller
{
    public function index(): \Inertia\Response
    {
        return Inertia::render('Books/Index', [
            'can_delete' => Auth::user()->can('delete books'),
            'books' => Book::with('authors')
                ->with('user')
                ->orderBy('title')
                ->paginate(10)
                ->through(fn ($book) => [
                    'id' => $book->id,
                    'title' => $book->title,
                    'authors' => $book->authors,
                    'user' => $book->user->name,
                    'deleted_at' => $book->deleted_at,
                ])
        ]);
    }

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Book $book): \Illuminate\Http\RedirectResponse
    {
        $this->authorize('delete', $book);

        $book->delete();

        return redirect()->back();
    }
}

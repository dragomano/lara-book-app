# Тестовое задание по Laravel

1. Установить laravel v8.5.20.
2. Установить spatie/laravel-permission
3. Сделать сидер ролей пользователей: юзер, менеджер, администратор.
4. Сделать сидер для модели «Книги» с использованием фабрики (название книги, автор, пользователь, который добавил книгу)
5. Сделать консольную команду для создания администратора.
6. Засидить 100 юзеров, 4 менеджера, 1000 книг.
7. На главной странице отобразить список книг с пагинацией (название, автор, пользователь). Добавить с помощью vue.js компонентов и axios возможность удалять книги только для администратора
8. Доступ к API только для авторизованных пользователей (реализовать любым удобным способом, учитывать сложность проекта). Возможность удалять книги - только для администратора.
9. Каждую минуту автоматически должна добавляться 1 книга. Нужно отслеживать данное событие и при добавлении книги присылать уведомление в группу в телеграмме, с сообщением: «Добавлена новая книга: «Название книги», «id пользователя»»
10. Сделать минимум 5 коммитов, залить на гит, предоставить ссылку на него.

## Реализация

1. Создаем папку проекта и устанавливаем в ней Laravel: `composer create-project laravel/laravel .`
2. Добавляем пакет [spatie/laravel-permission](https://spatie.be/docs/laravel-permission/):
   `composer require spatie/laravel-permission`. Создаем необходимую миграцию и публикуем файл настроек: `php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider"`
3. Создаем сидер для добавления администратора, 4 менеджеров и 100 пользователей, одновременно с назначением соответствующих полномочий каждому.
4. Создаем необходимые модели вместе с миграциями, с учётом двусторонней связи авторов и книг. Создаем фабрики AuthorFactory и BookFactory. Создаем сидер для добавления авторов и книг.
5. Создаем artisan-команду `create:admin {name} {email} {pass}` для добавления новых администраторов.
6. Запускаем миграции, заполняем таблицу данными:
   `php artisan migrate:refresh --seed`
7. Устанавливаем [Laravel Breeze](https://github.com/laravel/breeze): `composer require laravel/breeze --dev`, `php artisan breeze:install vue` и с помощью нескольких компонентов из [демонстрационного приложения PingCRM](https://inertiajs.ru/demo-application) создаем страницу управления книгами, с пагинацией.
8. Добавляем политику BookPolicy и проверку разрешения `Auth::user()->can('delete books')` в шаблоне. Теперь удалять книги смогут только администраторы.
9. Добавляем пакет [Telegram Notifications Channel](https://github.com/laravel-notification-channels/telegram): `composer require laravel-notification-channels/telegram`.
   Создаём уведомление: `php artisan make:notification TelegramNotification`. В app\Console\Kernel.php настраиваем ежеминутный запуск добавления фейковой книги. При успешном выполнении задачи отправляем сообщение в Телеграм:
```
$schedule->call(function () {
	Book::factory()->create();
})
	->everyMinute()
	->onSuccess(function () {
		$book = Book::with('user')->latest()->first();

		Notification::route(TelegramChannel::class, config('services.telegram-bot-api')['group'])
			->notify(new TelegramNotification([
				'text' => "Добавлена новая книга:\n«{$book->title}», {$book->user->name}"
			]));
});
```
В `config/services.php` добавляем:
```php
    'telegram-bot-api' => [
        'token' => env('TELEGRAM_BOT_TOKEN'),
        'group' => env('TELEGRAM_GROUP_ID'),
    ],
```
Не забываем указать токен нужного бота и идентификатор группы, в которую будет отправляться сообщение, в файле`.env`: **TELEGRAM_BOT_TOKEN** и **TELEGRAM_GROUP_ID**. Для проверки на локалке запускаем планировщик: `php artisan schedule:work`

![](demo.png)
